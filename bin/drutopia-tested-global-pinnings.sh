drupal/address                           1.9.0           Provides functionality for storing, validating and displaying international postal addresses.
drupal/admin_toolbar                     2.5.0           Provides a drop-down menu interface to the core Drupal Toolbar.
drupal/admin_toolbar_links_access_filter 3.1.0           Provides a workaround for the common problem that users with 'Use the administration pages and help' per...
drupal/admin_toolbar_tools               3.1.0           Adds menu links like Flush cache, Run cron, Run updates, and Logout under Drupal icon.
drupal/antibot                           1.5.0           Prevent forms from being submitted without JavaScript enabled.
drupal/autosave_form                     1.3.0           Adds autosave feature on forms.
drupal/better_normalizers                1.0.0-beta4     Improve the normalizers in core.
drupal/block_visibility_groups           1.4.0           Block Visibility Groups
drupal/bulma                             1.0.0-rc1       Built to use Bulma, a modern CSS framework based on Flexbox.
drupal/captcha                           1.2.0           The CAPTCHA module provides this feature to virtually any user facing web form on a Drupal site.
drupal/checklistapi                      1.11.0          Provides an API for creating fillable, persistent checklists.
drupal/ckeditorheight                    1.8.0           Adjusts CKEditor height to field row setting until we have https://www.drupal.org/node/2788905.
drupal/components                        2.4.0           Registers folders of components defined by your theme or module as Twig namespaces
drupal/config_actions                    1.3.0           Enables modules to provide actions that modify configuration.
drupal/config_actions_provider           1.0.0           Apply config actions when applying configuration updates.
drupal/config_distro                     1.0.0-alpha4    Framework for managing configuration updates from distributions.
drupal/config_distro_filter              1.0.0-alpha4    Bridge between Config Filter and Config Distro's use of Transform API to keep backwards compatibility.
drupal/config_filter                     2.2.0           Config Filter allows other modules to interact with a ConfigStorage through filter plugins.
drupal/config_merge                      1.0.0-rc3       Enables three-way merges of configuration items. This is useful for safely merging configuration updates...
drupal/config_normalizer                 1.0.0-alpha6    Normalizes configuration for comparison.
drupal/config_perms                      2.0.0           Allows additional permissions to be created and managed through an administration form.
drupal/config_provider                   2.0.0-rc4       Enables provision of configuration by extensions.
drupal/config_snapshot                   1.0.0-rc2       Manages snapshots of configuration as provided by modules and themes.
drupal/config_sync                       2.0.0-beta7     Synchronize your site with updated configuration provided by modules and themes.
drupal/config_update                     1.7.0           Provides basic revert and update functionality for other modules
drupal/core                              9.3.3           Drupal is an open source content management platform powering millions of websites and applications.
drupal/core-composer-scaffold            9.3.3           A flexible Composer project scaffold builder.
drupal/core-project-message              9.3.3           Adds a message after Composer installation.
drupal/core-recommended                  9.3.3           Locked core dependencies; require this project INSTEAD OF drupal/core.
drupal/crop                              2.1.0           Provides storage and API for image crops.
drupal/ctools                            3.7.0           Provides a number of utility and helper APIs for Drupal developers and site builders.
drupal/default_content                   1.0.0-alpha9    Imports default content when a module is enabled
drupal/drutopia_action                   1.1.0           Drutopia Action is a base feature providing an action content type and related configuration.
drupal/drutopia_article                  1.1.0           Drutopia Article is a base feature providing an article content type and related configuration.
drupal/drutopia_blog                     1.1.0           Drutopia Blog is a base feature providing a blog content type and related configuration.
drupal/drutopia_campaign                 1.1.0           Drutopia Campaign provides a Campaign content type, including background information as well as ability ...
drupal/drutopia_comment                  1.1.0           Drutopia comment is a base feature providing comments and related configuration.
drupal/drutopia_core                     1.2.0           Drutopia core is a base feature providing core components required by other features.
drupal/drutopia_event                    1.1.0           Drutopia Event is a base feature providing a event content type and related configuration.
drupal/drutopia_group                    1.1.0           Drutopia Group provides a group content type that can be classified by a group type vocabulary and relat...
drupal/drutopia_landing_page             1.2.0           Drutopia Landing Page is a base feature providing a landing page content type and related configuration.
drupal/drutopia_page                     1.1.0           Drutopia Page is a base feature providing a page content type and related configuration.
drupal/drutopia_people                   1.1.0           Drutopia People provides a People content type for showing visitors information about staff, volunteers,...
drupal/drutopia_related_content          1.1.0           Shows content (articles, actions, etc.) related to the current page's content based on the taxonomy term...
drupal/drutopia_resource                 1.1.0           Provides Resource content type and related configuration. A resource can be a file, such as a PDF, a lin...
drupal/drutopia_search                   1.1.0           Drutopia Search provides a search server, index, and view.
drupal/drutopia_seo                      1.1.0           Sensible Search Engine Optimization default configuration for a site.
drupal/drutopia_site                     1.1.0           Drutopia site is a base feature providing site components.
drupal/drutopia_social                   1.1.0           Drutopia Social provides a social media block, perhaps more in the future.
drupal/drutopia_storyline                1.1.0           Provides storyline paragraphs and an accompanying module to add a storyline to a basic page so that you ...
drupal/drutopia_user                     1.1.0           Drutopia user is a base feature providing user-related configuration.
drupal/ds                                3.13.0          Extend the display options for every entity type.
drupal/entity                            1.2.0           Provides expanded entity APIs, which will be moved to Drupal core one day.
drupal/entity_reference_revisions        1.9.0           Entity Reference Revisions
drupal/eu_cookie_compliance              1.19.0          This module aims at making the website compliant with the new EU cookie regulation.
drupal/exclude_node_title                1.3.0           Provides the option of excluding node title(s) from display by individual node, node bundle, and view mode.
drupal/facets                            1.8.0           The Facet module allows site builders to easily create and manage faceted search interfaces.
drupal/faqfield                          7.0.0           This module provides a field for frequently asked questions.
drupal/features                          3.12.0          Enables administrators to package configuration into modules
drupal/field_group                       3.2.0           Provides the field_group module.
drupal/focal_point                       1.5.0           Focal Point allows content creators to mark the most important part of an image for easier cropping.
drupal/gdpr                              dev-2.x 225604a Helps with making a site GDPR-compliant.
drupal/gnode                             1.4.0           Enables Group functionality for the Node module
drupal/group                             1.4.0           This module allows you to group users, content and other entities
drupal/honeypot                          2.0.2           Mitigates spam form submissions using the honeypot method.
drupal/jquery_ui                         1.4.0           Provides jQuery UI library.
drupal/jquery_ui_accordion               1.1.0           Provides jQuery UI Accordion library.
drupal/menu_admin_per_menu               1.3.0           Allows to give roles per menu admin permissions without giving them full administer menu permission.
drupal/menu_block                        1.7.0           Provides configurable blocks of menu links.
drupal/message                           1.2.0           Message
drupal/metatag                           1.19.0          Manage meta tags for all entities.
drupal/octavia                           1.3.0           A base theme for the Drutopia distribution based on Bulma.
drupal/paragraphs                        1.12.0          Enables the creation of Paragraphs entities.
drupal/paranoia                          1.0.0-rc1       Protects a site from some insecure configurations.
drupal/pathauto                          1.8.0           Provides a mechanism for modules to automatically generate aliases for the content they manage.
drupal/redirect                          1.7.0           Allows users to redirect from old URLs to new URLs.
drupal/redirect_404                      1.7.0           Logs 404 errors and allows users to create redirects for often requested but missing pages.
drupal/riddler                           1.2.0           Provides a question and answer CAPTCHA.
drupal/role_delegation                   1.1.0           Allows site administrators to grant some roles the authority to assign selected roles to users.
drupal/search_api                        1.23.0          Provides a generic framework for modules offering search capabilities.
drupal/search_api_db                     1.23.0          Offers an implementation of the Search API that uses database tables for indexing content.
drupal/similarterms                      1.5.0           Use Views to show similar content based on taxonomy terms
drupal/skins                             1.0.0-rc3       Enable themes or modules to provide a set of skins with distinct stylesheets and templates.
drupal/social_media_links                2.8.0           The module provides a block that display links (icons) to your profiles on various social networking sites.
drupal/subprofiles                       1.0.0           Provide multiple variations of a single installation profile.
drupal/token                             1.10.0          Provides a user interface for the Token API, some missing core tokens.
drupal/twigsuggest                       1.0.0-beta4     Provides template suggestions for things Drupal doesn't.
drupal/variationcache                    1.0.0           This project provides VariationCache, a redirect-aware caching implementation.
drupal/video_embed_field                 2.4.0           A pluggable field type for storing videos from external video hosts such as Vimeo and YouTube.
drupal/wysiwyg_linebreaks                1.11.0          Plugin to allow legacy content editing and more sane formatting in wysiwyg editors.
drupal/yaml_content                      1.0.0-alpha7    Demo content generator using yaml content templates.
